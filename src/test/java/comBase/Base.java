package comBase;

import Pages.*;
import Tools.Hooks;
import org.openqa.selenium.WebDriver;

public class Base {

    protected WebDriver driver;
    public Locationpreferences locationpreferences;
    public Cookies cookies;
    public Username_Password username_password;
    public LoginBTN loginBTN;
    public DepositBTN depositBTN;
    public BetHistory betHistory;
    public Transaction transaction;
    public WithdrawBTN withdrawBTN;
    public EditAccountInfo editAccountInfo;
    public Sportsmenu sportsmenu;
    public LIvegame_Menu lIvegame_menu;
    public Casino_Menu casino_menu;
    public JengaBets jengaBets;
    public Insights_Menu insights_menu;
    public LiveScore_Menu liveScore_menu;
    public News_Menu news_menu;
    public Results_Menu results_menu;
    public Search_Functionality search_functionality;
    public CloseSearchFunctionality closeSearchFunctionality;
    public HelpInformation helpInformation;
    public Furmula1Partner furmula1Partner;
    public Everton_Partner everton_partner;
    public HullPartner hullPartner;
    public SportpesapremierLeague sportpesapremierLeague;
    public LaLigaPartner laLigaPartner;
    public CorkCityPartner corkCityPartner;
    public FaceBook_Page faceBook_page;
    public Twitter_Page twitter_page;
    public YouTube_Page youTube_page;
    public Instagram_page instagram_page;
    public AboutUS_Page aboutUS_page;
    public AffliateJoinNow_Page affliateJoinNow_page;
    public AffiliateLogin_Page affiliateLogin_page;
    public MediaEnquiries_Page mediaEnquiries_page;
    public Sportpesanews sportpesanews;
    public Facebook_Sportpesa facebook_sportpesa;
    public Twitter_sportpesa twitter_sportpesa;
    public Youtube_Sportpesa youtube_sportpesa;
    public Instagram_Sportpesa instagram_sportpesa;
    public AffiliateJoin affiliateJoin;
    public Affiliatelogin affiliatelogin;
    public MediaEnquiries mediaEnquiries;
    public Virtual_menu virtual_menu;
    public App_menu app_menu;



    public Base() {
        this.driver = Hooks.driver;
        locationpreferences = new Locationpreferences(driver);
        cookies = new Cookies(driver);
        username_password = new Username_Password(driver);
        loginBTN = new LoginBTN(driver);
        depositBTN = new DepositBTN(driver);
        betHistory = new BetHistory(driver);
        transaction = new Transaction(driver);
        withdrawBTN = new WithdrawBTN(driver);
        editAccountInfo = new EditAccountInfo(driver);
        sportsmenu = new Sportsmenu(driver);
        lIvegame_menu = new LIvegame_Menu(driver);
        casino_menu = new Casino_Menu(driver);
        jengaBets = new JengaBets(driver);
        insights_menu = new Insights_Menu(driver);
        liveScore_menu = new LiveScore_Menu(driver);
        news_menu = new News_Menu(driver);
        results_menu = new Results_Menu(driver);
        search_functionality = new Search_Functionality(driver);
        closeSearchFunctionality = new CloseSearchFunctionality(driver);
        helpInformation = new HelpInformation(driver);
        furmula1Partner = new Furmula1Partner(driver);
        everton_partner = new Everton_Partner(driver);
        hullPartner = new HullPartner(driver);
        laLigaPartner = new LaLigaPartner(driver);
        sportpesapremierLeague = new SportpesapremierLeague(driver);
        corkCityPartner = new CorkCityPartner(driver);
        faceBook_page = new FaceBook_Page(driver);
        twitter_page = new Twitter_Page(driver);
        youTube_page = new YouTube_Page(driver);
        instagram_page = new Instagram_page(driver);
        aboutUS_page  = new AboutUS_Page(driver);
        affliateJoinNow_page = new AffliateJoinNow_Page(driver);
        affiliateLogin_page = new AffiliateLogin_Page(driver);
        mediaEnquiries_page = new MediaEnquiries_Page(driver);
        sportpesanews = new Sportpesanews(driver);
        facebook_sportpesa = new Facebook_Sportpesa(driver);
        twitter_sportpesa = new Twitter_sportpesa(driver);
        youtube_sportpesa = new Youtube_Sportpesa(driver);
        instagram_sportpesa = new Instagram_Sportpesa(driver);
        affiliateJoin  = new AffiliateJoin(driver);
        affiliatelogin = new Affiliatelogin(driver);
        mediaEnquiries = new MediaEnquiries(driver);
        virtual_menu= new Virtual_menu(driver);
        app_menu = new App_menu(driver);




    }
}
        /*logout = new LOGOUT(driver);
        sportsmenu = new Sportsmenu(driver);
        liveGames = new LiveGames(driver);
        jengaBets = new JengaBets(driver);
        insights = new Insights(driver);
        liveScore = new LiveScore(driver);
        registerButton = new RegisterButton(driver);
        forms = new Forms(driver);
        dropdown = new Dropdown(driver);
        locationPreferenceandCookie = new LocationPreferenceandCookie(driver);





    }
}*/