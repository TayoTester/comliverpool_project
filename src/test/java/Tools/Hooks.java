package Tools;

import org.junit.After;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

public class Hooks {

    public static WebDriver driver = null;
    @Test
    @cucumber.api.java.Before
    public WebDriver Registerpage () {


       // System.setProperty("webdriver.chrome.driver", "C:\\Users\\omotayo.akingboye.SPS\\Documents\\ChromeDriver\\chromedriver_win32 (1)\\chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        //driver.manage().window().setSize(new Dimension(414,736)); to get the size into mobile sizes
        driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
         driver.get("https://preprod.sportpesa.com/?sportId=1&section=highlights");
         //driver.get("https://www.sportpesa.com/");



        return driver;

    }

    @After
    public void teardown() {
        driver.quit();

    }


}



