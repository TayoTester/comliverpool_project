package Tools;

import Pages.*;
import comBase.Base;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class StepDefinition extends Base {
    @Given("Customer is on Sportpesahomepage")
    public void customer_is_on_Sportpesahomepage() {
        driver.get("https://preprod.sportpesa.com/");
        //driver.get("https://www.sportpesa.com/");
        Locationpreferences.SelectCountry.click();
        Cookies.CookiesClick.click();
    }

    @When("Customer enter valid username and password")
    public void customer_enter_valid_username_and_password() {
        Username_Password.Logindetails();
    }

    @When("Click away login Button")
    public void click_away_login_Button() {
        LoginBTN.Loginbtn2.click();

    }

    @Then("Customer should see account Transaction")
    public void customer_should_see_account_Transaction() {
        WebElement Transaction = driver.findElement(By.className("transactions"));
        Assert.assertEquals(true, Transaction.isDisplayed());
    }


    @Then("click Deposit button")
    public void customer_click_Deposit_button() {
        DepositBTN.Deposit.click();

    }


    @Then("click BetHistory")
    public void customer_click_BetHistory() {
        BetHistory.Bethistory1.click();

    }


    @Then("click Transaction")
    public void customer_click_Transaction() {
        Transaction.Transaction1.click();
    }

    @Then("click Withdraw")
    public void customer_click_Withdraw() {
        WithdrawBTN.Withdraw1.click();
    }


    @Then("click Edit Account info")
    public void customer_click_Edit_Account_info() {
        EditAccountInfo.Editinfo1.click();
    }

    @Then("click Sports menu")
    public void click_Sports_menu() {

        Sportsmenu.Sports1.click();
    }

    @Then("click LiveGames")
    public void customer_click_LiveGames() {
        LIvegame_Menu.Live1.click();
    }

    @And("^Click casino$")
    public void click_casino() throws Throwable {
        Casino_Menu.Casino1.click();
    }

    @And("^Click Virtual$")
    public void click_virtual() throws Throwable {
        Virtual_menu.Virtual1.click();
    }

    @Then("click JengaBets")
    public void click_JengaBets() {
        JengaBets.Jenga1.click();

    }

    @Then("click Insight menu")
    public void customer_click_Insight_menu() {
        Insights_Menu.Insight1.click();
    }

    @And("^click App$")
    public void click_app() {
        App_menu.App1.click();
    }


    @Then("click Search functionality")
    public void customer_click_Search_functionality() {
        Search_Functionality.Search1.click();

    }

    @Then("close Search Functionality Successfully")
    public void customer_close_Search_Functionality_Successfully() {
        CloseSearchFunctionality.CloseSearch1.click();

    }


    @Then("click Help Functionality")
    public void customer_click_Help_Functionality() {
        HelpInformation.Help1.click();

    }
}
//    @Then("click Formula{int} Team partner")
//    public void customer_click_Formula_Team_partner(Integer int1) {
//        Furmula1Partner.Formula1.click();
//
//    }
//
//    @Then("click Everton Partner")
//    public void customer_click_Everton_Partner() {
//        Everton_Partner.Everton1.click();
//    }
//
//    @Then("click Hull Partner")
//    public void customer_click_Hull_Partner() {
//        HullPartner.Hull1.click();
//
//    }
//
//    @Then("click LaLiga Partner")
//    public void customer_click_LaLiga_Partner() {
//        LaLigaPartner.Laliga1.click();
//
//    }
//
//    @Then("click sportPesa league")
//    public void click_sportPesa_league() {
//        SportpesapremierLeague.Sportpesa1.click();
//
//    }
//
//    @Then("click Cork City Partner")
//    public void customer_click_Cork_City_Partner() {
//        CorkCityPartner.Cork1.click();
//
//
//    }
//
//
//    @Then("click About US")
//    public void customer_click_About_US() {
//        AboutUS_Page.About1.click();
//
//
//    }
//    @Then("click SportpesaNews page")
//    public void click_SportpesaNews_page() {
//        Sportpesanews.News1.click();
//
//    }
//
//    @Then("click Facebook")
//    public void click_Facebook() {
//        Facebook_Sportpesa.Facebook1.click();
//
//    }
//
//    @Then("click Twitter")
//    public void click_insta() {
//        Twitter_sportpesa.Twitter1.click();
//
//    }
//
//    @Then("click Youtube")
//    public void click_Youtube() {
//        Youtube_Sportpesa.Youtube1.click();
//
//
//    }
//    @Then("click Instagram")
//    public void click_Instagram() {
//        Instagram_Sportpesa.Insta1.click();
//
//    }
//    @Then("click Affiliate-Join Now")
//    public void click_Affiliate_Join_Now() {
//        AffiliateJoin.Affiliatejoin1.click();
//
//    }
//
//    @Then("click Affiliate-Login")
//    public void click_Affiliate_Login() {
//        Affiliatelogin.Affiliatelogin1.click();
//
//    }
//
//    @Then("click Media Enquiries")
//    public void click_Media_Enquiries() {
//        MediaEnquiries.Media1.click();
//
//    }
//
//
//}
//
//




