package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LoginBTN {

        WebDriver driver = null;

        public LoginBTN (WebDriver driver) {
            this.driver = driver;
            PageFactory.initElements(driver, this);
            {
            }
        }

        @FindBy(xpath = "//*[@id=\"secondary_login\"]/input[4]") public static WebElement Loginbtn2;
        public static void Loginbtn3(){
            Loginbtn2.click();
        }
}
