package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AffiliateLogin_Page {

    WebDriver driver = null;

    public AffiliateLogin_Page(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
        {
        }
    }

    @FindBy(xpath= "/html/body/footer/section[2]/div/div/div[1]/ul/li[11]/a") public static WebElement Login1;
    public static void Login2(){
        Login1.click();
    }
}

