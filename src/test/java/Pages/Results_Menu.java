package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Results_Menu {

    WebDriver driver = null;

    public Results_Menu(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
        {
        }
    }

    @FindBy(partialLinkText = "Results") public static WebElement Results1;
    public static void Results2(){
        Results1.click();
    }
}

