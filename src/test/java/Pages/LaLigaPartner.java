package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LaLigaPartner {

    WebDriver driver = null;

    public LaLigaPartner(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
        {
        }
    }

    @FindBy(xpath= "/html/body/footer/section[1]/div/div/div/ul/li[3]/a") public static WebElement Laliga1;
    public static void Laliga2(){
        Laliga1.click();
    }
}

