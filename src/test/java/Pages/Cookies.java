package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Cookies {
    WebDriver driver = null;

    public Cookies(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
        {
        }
    }

    @FindBy(xpath = "//*[@id=\"cookies-law-info-content\"]/button")
    public static WebElement CookiesClick;

    public static void Cookiecheck() {

        CookiesClick.click();
    }

}



