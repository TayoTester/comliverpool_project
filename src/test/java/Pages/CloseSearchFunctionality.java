package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class CloseSearchFunctionality {

    WebDriver driver = null;

    public CloseSearchFunctionality(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
        {
        }
    }

    @FindBy(xpath= "/html/body/header/navigation-bar/div[2]/div/search-bar/div[1]/span[2]/span") public static WebElement CloseSearch1;

    public static void CloseSearch2(){

        CloseSearch1.click();
    }
}


