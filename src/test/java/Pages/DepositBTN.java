package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class DepositBTN {
    WebDriver driver = null;

    public DepositBTN(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
        {
        }
    }

    @FindBy(xpath= "/html/body/header/div[2]/div/div/div/div[3]/div[1]/balance/div/div[2]/a") public static WebElement Deposit;

    public static void Deposit2() {

        Deposit.click();
    }

}