package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Insights_Menu {

    WebDriver driver = null;

    public Insights_Menu(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
        {
        }
    }

    @FindBy(partialLinkText = "Insights") public static WebElement Insight1;
    public static void Insight2(){

        Insight1.click();
    }
}

