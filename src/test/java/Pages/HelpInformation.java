package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class HelpInformation {

    WebDriver driver = null;

    public HelpInformation(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
        {
        }
    }

    @FindBy(xpath= "/html/body/header/navigation-bar/div[1]/div/div/div/div[2]/a[2]") public static WebElement Help1;
    public static void Help2() {
        Help1.click();
    }
}

