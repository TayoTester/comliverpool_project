package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class YouTube_Page {

    WebDriver driver = null;

    public YouTube_Page(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
        {
        }
    }

    @FindBy(xpath= "/html/body/footer/section[2]/div/div/div[1]/ul/li[8]/a") public static WebElement Youtube1;
    public static void Youtube2(){
        Youtube1.click();
    }
}


