package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AboutUS_Page {

    WebDriver driver = null;

    public AboutUS_Page(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
        {
        }
    }

    @FindBy(xpath= "/html/body/footer/section[2]/div/div/div[1]/ul/li[4]/a") public static WebElement About1;

    public static void About2(){

        About1.click();
    }
}

