package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LiveScore_Menu {
    WebDriver driver = null;

    public LiveScore_Menu(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
        {
        }
    }

    @FindBy(partialLinkText = "LiveScore") public static WebElement Livescore1;
    public static void Livescore2(){
        Livescore1.click();
    }
}

