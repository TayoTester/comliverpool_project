package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class MediaEnquiries {

    WebDriver driver = null;

    public MediaEnquiries (WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
        {
        }
    }

    @FindBy(xpath= "/html/body/footer/section[2]/div/div/div[1]/ul/li[12]/a") public static WebElement Media1;

    public static void Media2() {
        Media1.click();
    }
}

