package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class CorkCityPartner {

    WebDriver driver = null;

    public CorkCityPartner(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
        {
        }
    }

    @FindBy(xpath= "/html/body/footer/section[1]/div/div/div/ul/li[5]/a") public static WebElement Cork1;
    public static void Cork2(){
        Cork1.click();
    }
}

