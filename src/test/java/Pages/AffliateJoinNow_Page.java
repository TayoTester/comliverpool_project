package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AffliateJoinNow_Page {

    WebDriver driver = null;

    public AffliateJoinNow_Page(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
        {
        }
    }

    @FindBy(xpath= "/html/body/footer/section[2]/div/div/div[1]/ul/li[10]/a") public static WebElement Join1;
    public static void Join2(){
        Join1.click();
    }
}

