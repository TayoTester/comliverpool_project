package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class App_menu {

    WebDriver driver = null;

    public App_menu(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
        {
        }
    }

    @FindBy(partialLinkText = "Apps") public static WebElement App1;

    public static void ClickApp() {
        App1.click();
    }
}


