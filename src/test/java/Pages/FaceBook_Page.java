package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class FaceBook_Page {

    WebDriver driver = null;

    public FaceBook_Page(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
        {
        }
    }

    @FindBy(xpath= "/html/body/footer/section[2]/div/div/div[1]/ul/li[6]/a") public static WebElement Facebook1;
    public static void Facebook2(){
        Facebook1.click();
    }
}

