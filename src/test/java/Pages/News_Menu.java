package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class News_Menu {
    WebDriver driver = null;

    public News_Menu(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
        {
        }
    }

    @FindBy(partialLinkText = "News") public static WebElement News1;
    public static void News2(){
        News1.click();
    }
}

