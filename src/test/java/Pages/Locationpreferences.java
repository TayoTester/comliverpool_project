package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Locationpreferences {
    WebDriver driver = null;

    public Locationpreferences(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
        {
        }
    }

    @FindBy(xpath = "/html/body/div[11]/p/section/div[1]/div/div[2]/ul/li[6]")
    public static WebElement SelectCountry;

    public static void Countryselection() {
        SelectCountry.click();

    }
}
